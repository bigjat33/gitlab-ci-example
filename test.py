import math
from rightTri import *
 
def test345solution():
	sides = [3,4,5] #given a 3 4 5 triangle
	expected_solution = [36.87,53.13,90] #expected side lengths
	actual_solution = rightTri(sides) #calculate the solution according to test function
	#verify actual solution meets expected solution within a tolerance
	verifyEqual1 = math.isclose(actual_solution[0], expected_solution[0], rel_tol=0.1)
	verifyEqual2 = math.isclose(actual_solution[1], expected_solution[1], rel_tol=0.1)
	verifyEqual3 = math.isclose(actual_solution[2], expected_solution[2], rel_tol=0.1)
	
	if (verifyEqual1 and verifyEqual2 and verifyEqual3):
		print("Test Passed")
	else:
		 raise Exception("Error in test345solution")
	

 
def test11root2solution():
	sides = [1,1,math.sqrt(2)]
	actual_solution = rightTri(sides)
	expected_solution = [45,45,90]
	verifyEqual1 = math.isclose(actual_solution[0], expected_solution[0], rel_tol=0.1)
	verifyEqual2 = math.isclose(actual_solution[1], expected_solution[1], rel_tol=0.1)
	verifyEqual3 = math.isclose(actual_solution[2], expected_solution[2], rel_tol=0.1)
	
	if (verifyEqual1 and verifyEqual2 and verifyEqual3):
		print("Test Passed")
	else:
		 raise Exception("Error in test11root2solution")
