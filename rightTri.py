# rightTri.py

# function
def rightTri(sides):
	import math

	A = math.degrees(math.atan(sides[0]/sides[1]))
	B = math.degrees(math.atan(sides[1]/sides[0]))
	hypotenuse = sides[0]/math.degrees(math.sin(A))
	C = math.degrees(math.asin(hypotenuse*math.degrees(math.sin(A))/sides[0]))

	angles = [A,B,C]
	return angles
